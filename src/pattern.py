# Import turtle and define tr
import turtle
tr = turtle.Turtle()
# Import random
import random

# Reset turtle for blank canvas
def reset():
    tr.clear()

# Setup turtle and all starting code
def setup():
    tr.speed(0)
    turtle.screensize(800, 1000, "Black")

# Function to draw the rectangle pattern
def drawRectanglePattern(centerX, centerY, offset, height, width, count, rotation):
    tr.left(rotation)
    for i in range(count):
        tr.penup()
        tr.goto(centerX, centerY)
        tr.forward(offset)
        drawRectangle(width, height)
        tr.left(360 / count)

# Function to draw a rectangle
def drawRectangle(width, height):
    setRandomColor()
    for i in range(2):
        tr.pendown()
        tr.forward(width)
        tr.left(90)
        tr.forward(height)
        tr.left(90)
        tr.penup()

# Function to draw the circle pattern
def drawCirclePattern(centerX, centerY, offset, radius, count):
    for i in range(count):
        setRandomColor()
        tr.penup()
        tr.goto(centerX, centerY)
        tr.forward(offset + radius)
        tr.pendown()
        tr.circle(radius)
        tr.left(360 / count)

# Function to draw the "super" pattern
def drawSuperPattern(num = 1):
    for i in range(num):
        pattern = random.randint(0, 1)
        if pattern == 0:
            drawRectanglePattern(random.randint(-400, 400), random.randint(-400, 400), random.randint(-150, 150),
            random.randint(10, 200), random.randint(10, 300), random.randint(1, 200), random.randint(0, 360))
        else:
            drawCirclePattern(random.randint(-400, 400), random.randint(-400, 400), random.randint(-150, 150),
            random.randint(5, 150), random.randint(1, 200))

# Function to set a random color for the shapes
def setRandomColor():
    color = random.randint(0, 5)
    if color == 0:
        tr.color("Green")
    elif color == 1:
        tr.color("Blue")
    elif color == 2:
        tr.color("Red")
    elif color == 3:
        tr.color("Yellow")
    elif color == 4:
        tr.color("Orange")
    else:
        tr.color("Purple")

# Once user is finished, this is executed to stop turtle
def done():
    turtle.done()