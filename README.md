# Pattern Drawer

## NOTE
This was an assignment done for CS1400 at Utah State University, some code from the project was done by Chad Mano as a basic for the assignment, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code.

## Manual
To work this program, after it is run follow the prompts for input on the type of shape you  would like to draw, how many, and any specifications asked. Then watch the Python Turtle create colorful patterns on the screen.
